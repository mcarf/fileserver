/*
 * Gestione operatori
 *
 * */

/* Numero operatori del server(espandibile) */
#define NOP 3

/* Struttura ausiliaria contenente pid e coda richieste di uno specifico operatore */
struct op {
  pid_t pid;
  int msgid;
};

/*
 * Ciclo infinito di un operatore
 *
 * funzione eseguita dopo fork() del server
 *  oper.pid   - pid operatore
 *  oper.msgid - coda richieste operatore
 *  opid       - id operatore
 *
 * */

void opCycle(struct op oper, int opid);
