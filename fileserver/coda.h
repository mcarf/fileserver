/*
 * Gestione code
 *
 * */


/* Struttura necessaria per msgsnd() e msgrcv() */
struct msgbuf {
  long mtype;
  char mtext[256]; /* 255 caratteri necessari per nome file in filesystem unix */
};

/* Chiave iniziale code operatori(i.e. op0 13, op1 14, op2 15)  */
#define MSGKEY 13

/* Apertura coda operatore
 *  opid - id operatore a cui aprire la coda
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int openQueueOp(int opid);


/* Apertura coda client
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int openQueueFile();


/* Chiusura coda
 *  msgid - id coda messaggi
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int closeQueue(int msgid);
