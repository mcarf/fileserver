#include <stdio.h>
#include <stdlib.h>

#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <signal.h>

#include <unistd.h>

#include "coda.h"
#include "semaforo.h"
#include "operator.h"
#include "orario.h"
#include "attesa.h"
#include "error.h"

static struct op ops[NOP]; /* array contenente pid e id coda degli operatori */
static int semid;   /* id set semafori */
static int wroomid; /* id waiting room */

void termination_handler(int sig_num) {
  int i; /* contatore operatori */

  printf("SIGQUIT ricevuto\n");

  for(i=0; i<NOP; i++) {
    printf("Server: Chiusura processo operatore %d... ", i);
    if( kill(ops[i].pid, SIGQUIT) == -1)
      errExit("chiusura processo");
    printf("OK\n");
    printf("Server: Deallocazione coda messaggi operatore %d... ", i);
    if( msgctl(ops[i].msgid, IPC_RMID, NULL)  == -1)
      errExit("chiusura coda operatore");
    printf("OK\n");
  }

  printf("Server: Rimozione stato sala d'attesa... ");
  if(deallocShWait(wroomid) == -1)
    errExit("rimozione stato attesa");
  printf("OK\n");

  printf("Server: Deallocazione semafori code... ");
  if(deallocSems(semid) == -1)
    errExit("deallocazione semafori");
  printf("OK\n");

  signal(SIGQUIT, SIG_DFL);
  kill(getpid(), SIGQUIT);
}

void alarm_handler(int sig_num) {
  int sopen;  /* secondi mancanti all'apertura */
  int sclose; /* secondi mancanti alla chiusura */

  printf("SIGALRM ricevuto\n");
  sopen = secondsToOpen();
  sclose = secondsToClose();
  if(sopen<sclose) {
    alarm(sopen);
    setWait(wroomid, 0);
    printf("Server: Sala d'attesa chiusa!\n");
  }
  else {
    alarm(sclose);
    setWait(wroomid, 1);
    printf("Server: Sala d'attesa aperta!\n");
  }
}


int main(int argc, char ** argv) {
  int i; /* contatore operatori */
  struct sigaction act; /* struttura ausiliaria per sigaction() */
  sigset_t mask; /* maschera segnali da ignorare */

  printf("Server: Apertura semafori per code... ");
  if((semid = allocSems()) == -1)
    errExit("allocazione semafori");
  printf("OK\n");

  printf("Server: Inizializzazione semafori... ");
  if(initSems(semid) == -1)
    errExit("inizializzazione semafori");
  printf("OK\n");

  printf("Server: Allocazione stato sala d'attesa... ");
  if((wroomid = allocShWait()) == -1)
    errExit("allocazione stato attesa");
  printf("OK\n");

  for(i=0; i<NOP; i++) {
    printf("Server: Allocazione coda messaggi operatore %d... ", i);
    if( (ops[i].msgid = openQueueOp(i)) == -1)
      errExit("apertura coda operatore");
    printf("OK\n");

    printf("Server: Fork operatore %d... ", i);
    fflush(stdout);
    switch(ops[i].pid = fork()) {
      case -1:
        errExit("fork");
      case 0:
        opCycle(ops[i], i);
      default:
        printf("OK\n");
    }
  }

  printf("Server: Modifica disposizione segnali... ");
  act.sa_handler = &termination_handler;
  if(sigaction(SIGQUIT, &act, NULL) == -1)
    errExit("disposizione SIGQUIT");
  act.sa_handler = &alarm_handler;
  if(sigaction(SIGALRM, &act, NULL) == -1)
    errExit("disposizione SIGALRM");
  printf("OK\n");

  /* imposto una maschera segnali senza SIGQUIT e SIGALRM */
  sigfillset(&mask);
  sigdelset(&mask, SIGQUIT);
  sigdelset(&mask, SIGALRM);

  /* settaggio iniziale sala d'attesa */
  alarm_handler(SIGALRM);

  printf("Server: Attesa segnali\n");

  while(1) {
    sigsuspend(&mask); /* blocco il processo in attesa di SIGQUIT o SIGALRM */
  }
  exit(EXIT_FAILURE);
}
