#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include "coda.h"

int openQueueOp(int opid) {
  return msgget(MSGKEY+opid, IPC_CREAT | IPC_EXCL | 0666);
}

int openQueueFile() {
  return msgget(IPC_PRIVATE, IPC_EXCL | 0666);
}

int closeQueue(int msgid) {
  return msgctl(msgid, IPC_RMID, NULL);
}

