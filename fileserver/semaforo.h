/*
 * Gestione semafori
 *
 * */

/* union necessaria per semctl() */
union semun {
  int 			val;
  struct semid_ds 	*buf;
  unsigned short 	*array;
#if defined(__linux__)
  struct seminfo 	*__buf;
#endif
};

#define SEMKEY 1460 /* Chiave set semafori condivisa */
#define SEMVAL 5 /* Valore inizializzazione semafori */

/* Allocazione set semafori
 * ritorna id del set semafori allocato in caso di successo, -1 altrimenti
 * */

int allocSems();

/* Inizializzazione valore semafori
 *  semid - id set semafori
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int initSems(int semid);

/* Deallocazione set semafori
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int deallocSems(int semid);

/* wait() su semaforo
 *  semId  - id set semafori
 *  semNum - numero semaforo
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int reserveSem(int semId, int semNum);

/* signal() su semaforo
 *  semId  - id set semafori
 *  semNum - numero semaforo
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int releaseSem(int semId, int semNum);
