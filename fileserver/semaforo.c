#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include "semaforo.h"
#include "operator.h"

int allocSems() {
  return semget(SEMKEY, NOP, IPC_CREAT | IPC_EXCL | 0666);
}

int initSems(int semid) {
  int i;
  union semun arg;

  arg.val = SEMVAL; /* inizializzo ogni semaforo del set a SETVAL */
  for(i=0; i<NOP; i++) {
    if( semctl(semid, i, SETVAL, arg) == -1)
      return -1;
  }
  return 0;
}

int deallocSems(int semid) {
  return semctl(semid, 0, IPC_RMID);
}

int reserveSem(int semId, int semNum) {
  struct sembuf sops;

  sops.sem_num = semNum;
  sops.sem_op = -1;
  sops.sem_flg = IPC_NOWAIT; /* non bloccante */

  return semop(semId, &sops, 1);
}

int releaseSem(int semId, int semNum) {
  struct sembuf sops;

  sops.sem_num = semNum;
  sops.sem_op = 1;
  sops.sem_flg = 0;

  return semop(semId, &sops, 1);
}

