#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>

#include "error.h"
#include "operator.h"

int main(int argc, char** argv) {
  int nclient; /* numero client da generare */
  int i;  /* contatore client*/
  int opid;    /* id operatore */
  int fcount;  /* numero file nella cartella dell'operatore */
  int fnumber; /* posizione file da recuperare */
  long int t;  /* timestamp in microsecondi */
  struct timeval tval; /* struttura ausiliaria per gettimeofday() */
  char buf[40]; /* stringa contenentte i comandi da passare a popen() ed execl() */
  char filename[258]; /* nome file cercato */
  FILE* pipe; /* stream della pipe */

  nclient = 0;
  if(argc == 2) {
    char* endptr;
    nclient = strtol(argv[1], &endptr, 0);
    if(endptr == argv[1] || nclient < 0) /* controllo se intero e positivo */
      errExit("numero client non valido");
  }
  else errExit("usage: gen <n_client>");


  /* ciclo generazione client */
  for(i=0; i<nclient; i++) {

    /* generazione seme per rand() utilizzando timestamp in microsecondi */
    gettimeofday(&tval, NULL);
    t = tval.tv_sec*1000000 + tval.tv_usec;
    srand(t);

    opid = rand() % NOP; /* scelta operatore random */

    sprintf(buf,"ls -al op%d | grep ^- | wc -l",opid); /* numero di file regolari nella cartella dell'operatore */
    if( (pipe = popen(buf, "r")) == NULL )
      errExit("popen");

    fcount = strtol(fgets(buf,40,pipe),  NULL, 0);
    pclose(pipe);

    fnumber = rand() % fcount; /* scelta file random */

    sprintf(buf, "ls -al op%d | grep ^- | awk '{print $9}'",opid); /* lista file regolari nella cartella dell'operatore */
     if( (pipe = popen(buf, "r")) == NULL )
      errExit("popen");
    
    /* cerco nome file in posizione fnumber */
    while(fscanf(pipe, "%s", filename) != EOF && fnumber>0)
      fnumber--;
    pclose(pipe);

    printf("client %d %s\n", opid, filename);

    sprintf(buf, "%d", opid); /* formato stringa per execl() */
    
    switch(fork()) {
      case -1:
        errExit("fork");
      case 0:
	if(execl("./client","client",buf,filename, NULL) == -1)
	  errExit("execl");
    }
  }
  exit(EXIT_SUCCESS);
}
