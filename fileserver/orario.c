#include <time.h>
#include "orario.h"


/* funzione generale indicante secondi mancanti ad ora h e minuti m  */
int secondsToEnd(int h, int m) {
  time_t now;
  struct tm *end;
  int diff;

  time(&now);
  end = localtime(&now);

  end->tm_hour = h;
  end->tm_min = m;
  end->tm_sec = 0;

  diff = (int)(difftime(mktime(end),now));

  return  diff<=0 ? diff+86400 : diff;
}

int secondsToOpen() {
  return secondsToEnd(OPENH,OPENM);
}

int secondsToClose() {
  return secondsToEnd(CLOSEH,CLOSEM);
}

