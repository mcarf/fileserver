#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <unistd.h>

#include "coda.h"
#include "semaforo.h"
#include "operator.h"
#include "attesa.h"
#include "error.h"


int main(int argc, char ** argv) {
  int opid = -1; /* id operatore */
  int semid;   /* id set semafori */
  int msgid;   /* id coda messaggi operatore */
  int wroomid; /* id sala d'attesa */
  int fileqid; /* id coda messaggi ricezione */
  pid_t clientpid;   /* pid client */
  struct msgbuf mex; /* buffer messaggi */

  if(argc == 3) {
    char *endptr; /* indirizzo del primo carattere non valido */
    opid = strtol(argv[1], &endptr, 0);
    if(endptr == argv[1] || opid < 0 || opid > NOP) /* controllo se intero e compreso nel range operatori */
      errExit("operatore invalido!");
  }
  else errExit("numero argomenti invalido!");

  clientpid = getpid();
  printf("Client %d: ricerca sala d'attesa... ", clientpid);
  if( (semid = semget(SEMKEY, 0, 0666)) == -1)
    errExit("sala d'attesa non raggiungibile");
  printf("OK\n");

  printf("Client %d: ricerca coda messaggi operatore %d... ", clientpid, opid);
  if( (msgid = msgget(MSGKEY+opid, 0666)) == -1)
    errExit("coda di messaggi non raggiungibile");
  printf("OK\n");

  printf("Client %d: ricerca stato sala d'attesa... ", clientpid);
  if( (wroomid = shmget(WROOMKEY, 0, 0666)) == -1)
    errExit("stato non raggiungibile");
  printf("OK\n");

  printf("Client %d: ingresso sala d'attesa... ", clientpid);
  if(isOpen(wroomid)<=0 || reserveSem(semid, opid) == -1) /* controllo se la sala d'attesa è aperta e non piena */
    errExit("sala d'attesa bloccata");
  printf("OK\n");

  printf("Client %d: Apertura coda contenuto file... ", clientpid);
  if( (fileqid = openQueueFile()) == -1)
    errExit("apertura coda client");
  printf("OK\n");

  /* mex.mtext - stringa file richiesto
   * mex.mtype - id coda ricezione testo
   * */
  strcpy(mex.mtext,argv[2]);
  mex.mtype = fileqid;

  printf("Client %d: Invio richiesta file... ", clientpid);
  if( msgsnd(msgid, &mex, sizeof(mex), 0) == -1)
    errExit("invio richiesta file");
  printf("OK\n");

  printf("Client %d: Testo file:\n\n", clientpid);

  /* ciclo buffering testo */
  do {
    if( msgrcv(fileqid, &mex, sizeof(mex), 0, 0) == -1)
      errExit("ricezione contenuto file");
    printf("Client %d: %s\n", clientpid, mex.mtext);
  } while(mex.mtext[0] != '\0'); /* scelgo \0 come carattere di fine trasmissione */

  printf("\nClient %d: Uscita sala d'attesa... ", clientpid);
  if( (releaseSem(semid,opid)) == -1)
    errExit("release semaforo");
  printf("OK\n");

  printf("Client %d: Chiusura coda contenuto file... ", clientpid);
  if(closeQueue(fileqid) == -1)
    errExit("chiusura coda client");
  printf("OK\n");

  exit(EXIT_SUCCESS);
}
