/*
 * Gestione orario apertura e chiusura sala d'attesa
 *
 * */

/* Ora e minuti di apertura */
#define OPENH 0
#define OPENM 0

/* Ora e minuti di chiusura */
#define CLOSEH 23
#define CLOSEM 59

/* Secondi all'apertura della sala d'attesa */
int secondsToOpen();

/* Secondi alla chiusura della sala d'attesa */
int secondsToClose();
