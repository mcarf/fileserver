/*
 * Gestione stato sala d'attesa
 *
 * */

/* Chiave per memoria condivisa */
#define WROOMKEY 35

/* Allocazione memoria condivisa
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int allocShWait();

/* Deallocazione memoria condivisa 
 *  wroomid - waiting room id
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int deallocShWait(int wroomid);

/* Settaggio sala d'attesa
 *  wroomid - waiting room id
 *  val - valore booleano indicante stato sala d'attesa
 * ritorna 0 in caso di successo, -1 altrimenti
 * */

int setWait(int wroomid, int val);

/* Controllo stato sala d'attesa 
 *  wroomid - waitng room id
 *
 * ritorna:
 *  sala aperta 1
 *  sala chiusa 0
 *  errore     -1
 * */

int isOpen(int wroomid);
