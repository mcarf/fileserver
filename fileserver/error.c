#include <stdio.h>
#include <stdlib.h>

void errExit(char *err) {
  printf("error: %s!\n", err);
  exit(EXIT_FAILURE);
}
