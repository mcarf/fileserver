#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "operator.h"
#include "coda.h"
#include "error.h"

void opCycle(struct op oper, int opid) {

  int fileqid, cread;
  char path[260];
  struct msgbuf request;
  FILE* dati = NULL;

  while(1) { /* ciclo infinito operatore */

    printf("OP%d: Attesa richiesta...\n", opid);
    fflush(stdout); /* necessario in quanto la stampa di printf() è ancora in buffer durante l'attesa di msgrcv() */
    if( msgrcv(oper.msgid, &request, sizeof(request), 0, 0) == -1)
      errExit("richiesta non ricevuta");

  
    printf("OP%d: Elaborazione richiesta...\n", opid);
    fileqid = request.mtype;
    sprintf(path,"op%d/%s",opid,request.mtext); /* path contiene percorso relativo a opN/nomefile */

    if((dati = fopen(path, "r")) != NULL) {
      printf("OP%d: File trovato! Elaborazione...\n", opid);
      sleep(5); /* attesa necessaria per test semafori */
      printf("Invio dati testuali...\n");

      /* ciclo invio dati in buffer */
      while(!feof(dati)) {
        cread = fread(request.mtext, sizeof(char), 255, dati);
        request.mtext[cread] = '\0';
        if( msgsnd(fileqid, &request, sizeof(request), IPC_NOWAIT) == -1)
          errExit("invio contenuto");
      }
      fclose(dati);
    }
    else {
      printf("OP%d: File non trovato! Invio messaggio d'errore... ", opid);
      strcpy(request.mtext, "file non esistente!");
      if( msgsnd(fileqid, &request, sizeof(request), IPC_NOWAIT) == -1)
        errExit("invio contenuto");
    }
    
    /* scelgo \0 come carattere per fine trasmissione */
    strcpy(request.mtext,"");
    if( msgsnd(fileqid, &request, sizeof(request), IPC_NOWAIT) == -1)
      errExit("invio contenuto");
    printf("OK\n");
  }
}

