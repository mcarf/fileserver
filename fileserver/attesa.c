#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

#include "attesa.h"


int allocShWait() {
  return shmget(WROOMKEY, sizeof(int), IPC_CREAT | IPC_EXCL | 0666);
}

int deallocShWait(int wroomid) {
  return shmctl(wroomid, IPC_RMID, NULL);
}

int setWait(int wroomid, int val) {
  int* wroom;
  wroom = (int*) shmat(wroomid, NULL, 0);
  if (wroom != (int*)(-1))
    *wroom = val;
  else return -1; /* errore in fase di attachment */
  return shmdt(wroom); /* -1 in caso di errore in detachment, 0 altrimenti */
}

int isOpen(int wroomid) {
  int* wroom, val;
  wroom = (int*) shmat(wroomid, NULL, 0);
  if(wroom != (int*)(-1))
    val = *wroom;
  else return -1; /* errore in fase di attachment */
  return (shmdt(wroom) != -1) ? val : -1; /* -1 in caso di errore in detachment, stato sala d'attesa(0/1) altrimenti */
}
